import { Field, ID, InputType } from 'type-graphql';

@InputType()
export class AddUserRolesDTO {
  @Field(type => ID)
  id: string;
  @Field(type => [ID!]!, { nullable: false })
  roles: string[];
}
