export class AddUserDTO {
  username: string;
  password: string;
  email: string;
  firstName: string;
  lastName: string;
}
