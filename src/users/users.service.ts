import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { IUser } from './interfaces/user.interface';
import { AddUserDTO } from './dtos/add-user.dto';
import { AddUserRolesDTO } from './dtos/add-user-roles.dto';
import { AddUserOperationsDTO } from './dtos/add-user-operations.dto';
import { UserPaginatedResponse } from './models/user-paginated-response';
import { QueryField } from '../interfaces/query-field';
import { ListQueryArgs } from '../models/list-query-args';
import { QueryHelper } from '../helpers/query.helper';

@Injectable()
export class UsersService {
  constructor(@InjectModel('User') private readonly userModel: Model<IUser>) {}

  async findById(id: string): Promise<IUser> {
    return await this.userModel.findById(id).exec();
  }

  async findOneByUsername(username: string, fields: any = {}): Promise<IUser> {
    const caseInsensitiveUsername = new RegExp(`^${username}$`, 'i');
    return await this.userModel.findOne({
      $or: [
        { username: caseInsensitiveUsername },
        { email: caseInsensitiveUsername },
      ],
    }, fields).exec();
  }

  async createUser(user: AddUserDTO): Promise<IUser> {
    const createdUser = new this.userModel(user);
    return await createdUser.save();
  }

  async updateLastLoggedIn(id: string) {
    this.userModel.updateOne({ _id: id }, { lastLoggedInAt: Date.now(), loginAttempts: 0 }).exec();
  }

  async updateLoginAttemps(id: string, attempts: number) {
    const loginAttempts = attempts + 1;
    if (loginAttempts >= 5) {
        this.userModel.updateOne({ _id: id }, { locked: true }).exec();
    } else {
        this.userModel.updateOne({ _id: id }, { loginAttempts }).exec();
    }
  }

  async findAllPaginated(fields: QueryField, { skip, take, sorts }: ListQueryArgs): Promise<UserPaginatedResponse> {
    const projection = fields || {};
    const sortFields = QueryHelper.generateSorts(sorts);
    let query = this.userModel.find({}, projection, { skip: (skip * take), limit: take, sort: sortFields });
    query = QueryHelper.generatePopulationQuery(query, ['roles'], fields);
    const items = await query.exec();
    const total =  await this.userModel.countDocuments({});
    const hasMore = (skip + 1) * take < total;
    return {
      total,
      items,
      hasMore,
    };
  }

  async addOperations({ id, operations }: AddUserOperationsDTO ): Promise<IUser> {
    try {
      return await this.userModel.findByIdAndUpdate(id, { $addToSet: { operations } }, { new: true });
    } catch (exception) {
      throw exception;
    }
  }

  async addRoles({ id, roles }: AddUserRolesDTO ): Promise<IUser> {
    try {
      return await this.userModel.findByIdAndUpdate(id, { $addToSet: { roles } }, { new: true });
    } catch (exception) {
      throw exception;
    }
  }
}
