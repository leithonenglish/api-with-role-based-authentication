import { Schema } from 'mongoose';

export const UserSchema = new Schema({
  username: {
    type: String,
    index: true,
  },
  password: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  firstName: {
    type: String,
    required: true,
  },
  lastName: {
    type: String,
    required: true,
  },
  active: {
    type: Boolean,
    default: false,
  },
  locked: {
    type: Boolean,
    default: false,
  },
  lastLoggedInAt: {
    type: Date,
  },
  loginAttempts: {
    type: Number,
    default: 0,
  },
  operations: {
    type: [Number],
  },
  roles: {
    type: [
      {
        type: Schema.Types.ObjectId,
        ref: 'Role',
      },
    ],
  },
}, { timestamps: {} });
