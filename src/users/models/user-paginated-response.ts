import { ObjectType } from 'type-graphql';
import PaginatedResponse from '../../models/paginated-response';
import { User } from './user';

@ObjectType()
export class UserPaginatedResponse extends PaginatedResponse(User) { }
