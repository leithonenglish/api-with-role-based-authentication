import { ObjectType, Field, ID } from 'type-graphql';
import { Role } from '../../roles/models/role';
import { Operation } from '../../enums/operation.enum';

@ObjectType()
export class User {
  @Field(type => ID, { nullable: false })
  id: string;
  @Field({ nullable: false })
  username: string;
  password: string;
  @Field({ nullable: false })
  email: string;
  @Field({ nullable: false })
  firstName: string;
  @Field({ nullable: false })
  lastName: string;
  @Field({ nullable: false })
  active: boolean;
  @Field({ nullable: false })
  locked: boolean;
  @Field({ nullable: false })
  lastLoggedInAt: Date;
  @Field({ nullable: false })
  loginAttempts: number;
  @Field({ nullable: false })
  createdAt: Date;
  @Field({ nullable: false })
  updatedAt: Date;
  @Field(type => [Operation!], { nullable: true })
  operations?: Operation[];
  @Field(type => [Role!], { nullable: true })
  roles?: Role[];
}
