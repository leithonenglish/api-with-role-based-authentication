import { Document } from 'mongoose';
import { Role } from '../../roles/models/role';
import { Operation } from '../../enums/operation.enum';

export interface IUser extends Document {
  id: string;
  username: string;
  password: string;
  email: string;
  firstName: string;
  lastName: string;
  active: boolean;
  locked: boolean;
  lastLoggedInAt: Date;
  loginAttempts: number;
  createdAt: Date;
  updatedAt: Date;
  operations?: Operation[];
  roles?: Role[];
}
