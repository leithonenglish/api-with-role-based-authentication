import { Resolver, Query, Args, Info, Mutation } from '@nestjs/graphql';
import { GraphQLResolveInfo } from 'graphql';
import { UsersService } from './users.service';
import { User } from './models/user';
import { UserPaginatedResponse } from './models/user-paginated-response';
import { ListQueryArgs } from '../models/list-query-args';
import { QueryHelper } from '../helpers/query.helper';
import { AddUserOperationsDTO } from './dtos/add-user-operations.dto';
import { AddUserRolesDTO } from './dtos/add-user-roles.dto';

@Resolver(of => User)
export class UsersResolver {
  constructor(private readonly usersService: UsersService) {}

  @Query(returns => UserPaginatedResponse)
  async users(@Args() args: ListQueryArgs, @Info() info: GraphQLResolveInfo): Promise<UserPaginatedResponse> {
    const fields = QueryHelper.getFieldsFromResolver(info, 'items');
    return await this.usersService.findAllPaginated(fields, args);
  }

  @Mutation(returns => User)
  async addOperationsToUser(@Args('args') args: AddUserOperationsDTO) {
    return await this.usersService.addOperations(args);
  }

  @Mutation(returns => User)
  async addRolesToUser(@Args('args') args: AddUserRolesDTO) {
    return await this.usersService.addRoles(args);
  }
}
