import { registerEnumType } from 'type-graphql';

export enum Operation {
  CREATE_USER,
  EDIT_USER,
  VIEW_USER,
  DEACTIVATE_USER,
  DELETE_USER,
  VIEW_ROLES,
  CREATE_ROLE,
  EDIT_ROLE,
  REMOVE_ROLE,
  ASSIGN_ROLE,
  EDIT_ROLE_OPERATION,
  EDIT_USER_OPERATION,
  VIEW_TEMPLATE,
  ADD_TEMPLATE,
  DELETE_TEMPLATE,
  EDIT_TEMPLATE,
}

registerEnumType(Operation, { name: 'Operation' });
