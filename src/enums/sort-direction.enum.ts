export enum SortDirection {
  ASCENDING = 1,
  DESCENDING = -1,
}
