import * as graphqlFields from 'graphql-fields';
import * as dot from 'dot-object';
import * as _ from 'lodash';
import { Model, DocumentQuery, Document } from 'mongoose';
import { GraphQLResolveInfo } from 'graphql';
import { QueryField } from '../interfaces/query-field';
import { SortOption } from 'src/models/sort-option';

export class QueryHelper {
  static getFieldsFromResolver(info: GraphQLResolveInfo, sudoParent?: string): QueryField {
    const fieldStructure = graphqlFields(info);
    const fields = dot.dot(fieldStructure);
    const projections: QueryField = { };
    Object.keys(fields).forEach((key) => {
      const prop = sudoParent ? key.replace(`${sudoParent}.`, '') : key;
      projections[prop] = true;
    });
    return projections;
  }

  static getFieldsForPopulationProjection(ref: string, fields: QueryField) {
    return Object.keys(fields)
    .filter((key) => key.includes(ref))
    .map((key) => key.replace(`${ ref }.`, ''))
    .join(' ');
  }

  static generatePopulationQuery<T>(query: DocumentQuery<T, any>, refs: string[], fields: QueryField) {
    refs.forEach((ref) => {
      if (_.some(Object.keys(fields), (key) => key.includes(ref))) {
        const selectedFields = this.getFieldsForPopulationProjection(ref, fields);
        query = query.populate({ path: ref, select: selectedFields });
      }
    });
    return query;
  }

  static generateSorts(sorts: SortOption[]) {
    if (sorts) {
      const sortFields = { };
      sorts.forEach((sort) => {
        sortFields[sort.field] = sort.direction;
      });
      return sortFields;
    }
    return { };
  }
}
