import { Field, ObjectType, ID } from 'type-graphql';

@ObjectType()
export class Template {
  @Field(type => ID, { nullable: false })
  id: string;
  @Field({ nullable: false })
  title: string;
  @Field({ nullable: false })
  description: string;
  @Field(type => [String!], { nullable: true })
  variables: string[];
  @Field({ nullable: false })
  source: string;
}
