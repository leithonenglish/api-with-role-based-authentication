import { Schema } from 'mongoose';

export const TemplateSchema = new Schema({
  title: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  variables: {
    type: [String],
    required: true,
  },
  source: {
    type: String,
    required: true,
  },
});
