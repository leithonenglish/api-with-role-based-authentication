import { Document } from 'mongoose';

export default interface ITemplate extends Document {
  id: string;
  title: string;
  description: string;
  variables: string[];
  source: string;
}
