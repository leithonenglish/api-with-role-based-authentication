import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { TemplatesService } from './templates.service';
import { TemplatesResolver } from './templates.resolver';
import { TemplateSchema } from './schemas/template.schema';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'Template', schema: TemplateSchema }])],
  providers: [TemplatesResolver, TemplatesService],
})
export class TemplatesModule {}
