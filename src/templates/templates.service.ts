import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import ITemplate from './interfaces/template.interface';
import { CreateTemplateDTO } from './dtos/create-template.dto';
import { UpdateTemplateDTO } from './dtos/update-template.dto';
import { QueryField } from '../interfaces/query-field';

@Injectable()
export class TemplatesService {
  constructor(@InjectModel('Template') private readonly templateModel: Model<ITemplate>) {}

  async findAll(fields: QueryField): Promise<ITemplate[]> {
    const projection = fields || {};
    const query = this.templateModel.find({}, projection);
    return await query.exec();
  }

  async findOneById(fields: QueryField, id: string): Promise<ITemplate> {
    const projection = fields || {};
    const query = this.templateModel.findById(id, projection);
    return await query.exec();
  }

  async add(template: CreateTemplateDTO): Promise<ITemplate> {
    const newTemplate = new this.templateModel(template);
    return await newTemplate.save();
  }

  async update(template: UpdateTemplateDTO): Promise<ITemplate> {
    try {
      return await this.templateModel.findByIdAndUpdate(template.id, template, { new: true });
    } catch (exception) {
      throw exception;
    }
  }

  async delete(id: string): Promise<ITemplate> {
    try {
      return await this.templateModel.findByIdAndDelete(id);
    } catch (exception) {
      throw exception;
    }
  }
}
