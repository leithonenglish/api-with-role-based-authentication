import { Field, InputType } from 'type-graphql';

@InputType()
export class CreateTemplateDTO {
  @Field({ nullable: false })
  title: string;
  @Field({ nullable: false })
  description: string;
  @Field(type => [String]!, { nullable: true })
  variables: string[];
  @Field({ nullable: false })
  source: string;
}
