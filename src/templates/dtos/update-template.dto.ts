import { InputType, Field, ID } from 'type-graphql';
import { CreateTemplateDTO } from './create-template.dto';

@InputType()
export class UpdateTemplateDTO extends CreateTemplateDTO {
  @Field(type => ID, { nullable: false })
  id: string;
}
