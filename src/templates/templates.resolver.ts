import { Resolver, Query, Args, Info, Mutation } from '@nestjs/graphql';
import { GraphQLResolveInfo } from 'graphql';
import { TemplatesService } from './templates.service';
import { Template } from './models/template';
import { UpdateTemplateDTO } from './dtos/update-template.dto';
import { CreateTemplateDTO } from './dtos/create-template.dto';
import { QueryHelper } from '../helpers/query.helper';

@Resolver(of => Template)
export class TemplatesResolver {
  constructor(private readonly templatesService: TemplatesService) {}

  @Query(returns => [Template])
  async templates(@Info() info: GraphQLResolveInfo): Promise<Template[]> {
    const fields = QueryHelper.getFieldsFromResolver(info, 'items');
    return await this.templatesService.findAll(fields);
  }

  @Query(returns => Template)
  async template(@Args({ name: 'id', type: () => String }) id: string, @Info() info: GraphQLResolveInfo): Promise<Template> {
    const fields = QueryHelper.getFieldsFromResolver(info, 'items');
    return await this.templatesService.findOneById(fields, id);
  }

  @Mutation(returns => Template)
  async createTemplate(@Args('template') template: CreateTemplateDTO): Promise<Template> {
    return await this.templatesService.add(template);
  }

  @Mutation(returns => Template)
  async updateTemplate(@Args('template') template: UpdateTemplateDTO): Promise<Template> {
    return await this.templatesService.update(template);
  }

  @Mutation(returns => Template)
  async deleteTemplate(@Args({ name: 'id', type: () => String }) id: string): Promise<Template> {
    return await this.templatesService.delete(id);
  }
}
