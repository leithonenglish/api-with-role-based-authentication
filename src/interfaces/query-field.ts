export interface QueryField {
  [key: string]: boolean;
}
