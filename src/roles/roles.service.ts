import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { QueryField } from '../interfaces/query-field';
import { ListQueryArgs } from '../models/list-query-args';
import { QueryHelper } from '../helpers/query.helper';
import { RolePaginatedResponse } from './models/role-paginated-response';
import IRole from './interfaces/role.interface';
import { AddRoleOperationsDTO } from './dtos/add-role-operations.dto';
import { CreateRoleDTO } from './dtos/create-role.dto';
import { UpdateRoleDTO } from './dtos/update-role.dto';

@Injectable()
export class RolesService {
  constructor(@InjectModel('Role') private readonly roleModel: Model<IRole>) {}

  async findAllPaginated(fields: QueryField, { skip, take, sorts }: ListQueryArgs): Promise<RolePaginatedResponse> {
    const projection = fields || {};
    const sortFields = QueryHelper.generateSorts(sorts);
    const query = this.roleModel.find({}, projection, { skip: (skip * take), limit: take, sort: sortFields });
    const items = await query.exec();
    const total =  await this.roleModel.countDocuments({});
    const hasMore = (skip + 1) * take < total;
    return {
      total,
      items,
      hasMore,
    };
  }

  async add(role: CreateRoleDTO): Promise<IRole> {
    const newRole = new this.roleModel(role);
    return await newRole.save();
  }

  async update(role: UpdateRoleDTO): Promise<IRole> {
    try {
      return await this.roleModel.findByIdAndUpdate(role.id, role, { new: true });
    } catch (exception) {
      throw exception;
    }
  }

  async findOneById(fields: QueryField, id: string): Promise<IRole> {
    const projection = fields || {};
    const query = this.roleModel.findById(id, projection);
    return await query.exec();
  }

  async delete(id: string): Promise<IRole> {
    try {
      return await this.roleModel.findByIdAndDelete(id);
    } catch (exception) {
      throw exception;
    }
  }

  async addOperations({ id, operations }: AddRoleOperationsDTO ): Promise<IRole> {
    try {
      return await this.roleModel.findByIdAndUpdate(id, { $addToSet: { operations } }, { new: true });
    } catch (exception) {
      throw exception;
    }
  }
}
