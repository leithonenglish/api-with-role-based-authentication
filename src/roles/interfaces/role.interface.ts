import { Document } from 'mongoose';
import { Operation } from '../../enums/operation.enum';

export default interface IRole extends Document {
  id: string;
  name: string;
  description: string;
  operations: Operation[];
}
