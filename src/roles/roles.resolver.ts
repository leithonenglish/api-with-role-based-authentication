import { Resolver, Query, Args, Info, Mutation } from '@nestjs/graphql';
import { GraphQLResolveInfo } from 'graphql';
import { RolesService } from './roles.service';
import { Role } from './models/role';
import { CreateRoleDTO } from './dtos/create-role.dto';
import { UpdateRoleDTO } from './dtos/update-role.dto';
import { RolePaginatedResponse } from './models/role-paginated-response';
import { AddRoleOperationsDTO } from './dtos/add-role-operations.dto';
import { ListQueryArgs } from '../models/list-query-args';
import { QueryHelper } from '../helpers/query.helper';

@Resolver(of => Role)
export class RolesResolver {
  constructor(private readonly rolesService: RolesService) {}

  @Query(returns => RolePaginatedResponse)
  async roles(@Args() args: ListQueryArgs, @Info() info: GraphQLResolveInfo): Promise<RolePaginatedResponse> {
    const fields = QueryHelper.getFieldsFromResolver(info, 'items');
    return await this.rolesService.findAllPaginated(fields, args);
  }

  @Mutation(returns => Role)
  async createRole(@Args('role') role: CreateRoleDTO): Promise<Role> {
    return await this.rolesService.add(role);
  }

  @Mutation(returns => Role)
  async updateRole(@Args('role') role: UpdateRoleDTO): Promise<Role> {
    return await this.rolesService.update(role);
  }

  @Query(returns => Role)
  async role(@Args({ name: 'id', type: () => String }) id: string, @Info() info: GraphQLResolveInfo): Promise<Role> {
    const fields = QueryHelper.getFieldsFromResolver(info, 'items');
    return await this.rolesService.findOneById(fields, id);
  }

  @Mutation(returns => Role)
  async deleteRole(@Args({ name: 'id', type: () => String }) id: string): Promise<Role> {
    return await this.rolesService.delete(id);
  }

  @Mutation(returns => Role, { nullable: true })
  async addOperationsToRole(@Args('args') args: AddRoleOperationsDTO) {
    return await this.rolesService.addOperations(args);
  }
}
