import { Field, ObjectType, ID } from 'type-graphql';
import { Operation } from '../../enums/operation.enum';

@ObjectType()
export class Role {
  @Field(type => ID, { nullable: false })
  id: string;
  @Field({ nullable: false })
  name: string;
  @Field({ nullable: false })
  description: string;
  @Field(type => [Operation!]!, { nullable: false })
  operations: Operation[];
}
