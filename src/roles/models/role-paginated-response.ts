import { ObjectType } from 'type-graphql';
import PaginatedResponse from '../../models/paginated-response';
import { Role } from './role';

@ObjectType()
export class RolePaginatedResponse extends PaginatedResponse(Role) { }
