import { Schema } from 'mongoose';

export const RoleSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  operations: {
    type: [Number],
    required: true,
  },
});
