import { InputType, Field, ID } from 'type-graphql';
import { CreateRoleDTO } from './create-role.dto';

@InputType()
export class UpdateRoleDTO extends CreateRoleDTO {
  @Field(type => ID, { nullable: false })
  id: string;
}
