import { Field, InputType } from 'type-graphql';
import { Operation } from '../../enums/operation.enum';

@InputType()
export class CreateRoleDTO {
  @Field({ nullable: false })
  name: string;
  @Field({ nullable: false })
  description: string;
  @Field(type => [Operation!]!, { nullable: false })
  operations: Operation[];
}
