import { Field, ID, InputType } from 'type-graphql';
import { Operation } from '../../enums/operation.enum';

@InputType()
export class AddRoleOperationsDTO {
  @Field(type => ID)
  id: string;
  @Field(type => [Operation!]!, { nullable: false })
  operations: Operation[];
}
