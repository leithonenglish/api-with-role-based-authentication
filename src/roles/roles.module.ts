import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { RolesService } from './roles.service';
import { RoleSchema } from './schemas/role.schema';
import { RolesResolver } from './roles.resolver';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'Role', schema: RoleSchema }])],
  providers: [RolesService, RolesResolver],
})
export class RolesModule {}
