import { Field, Int, ObjectType, InputType } from 'type-graphql';
import { SortDirection } from '../enums/sort-direction.enum';

@ObjectType()
@InputType('SortOptionInput')
export class SortOption {
  @Field()
  field: string;
  @Field(type => Int)
  direction: SortDirection;
}
