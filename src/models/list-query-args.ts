import { Min, Max } from 'class-validator';
import { ArgsType, Field, Int, InputType } from 'type-graphql';
import { SortOption } from './sort-option';

@ArgsType()
export class ListQueryArgs {
  @Field(type => Int, { defaultValue: 0 })
  @Min(0)
  skip: number;

  @Field(type => Int)
  @Min(1)
  @Max(50)
  take = 10;

  @Field(type => [SortOption], { defaultValue: [] })
  sorts: SortOption[];
}
