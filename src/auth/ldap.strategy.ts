import * as Strategy from 'passport-ldapauth';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { AuthService } from './auth.service';

@Injectable()
export class LdapStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly authService: AuthService) {
    super(async (req, callback) => {
      const opts = {
        server : {
          url: 'ldap://localhost:389',
          bindDn: 'cn=admin,dc=example,dc=org',
          bindCredentials: 'secret',
          searchBase: 'dc=example,dc=org',
          searchFilter: '(uid={{username}})',
        },
      };
      callback(null, opts);
    });
  }

  async validate(response: any): Promise<any> {
    const { cn } = response;
    const user = await this.authService.getUserByUsername(cn);
    if (!user) {
      throw new UnauthorizedException();
    }
    return user;
  }
}
