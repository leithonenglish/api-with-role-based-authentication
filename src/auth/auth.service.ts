import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcryptjs';
import { UsersService } from '../users/users.service';
import { User } from '../users/models/user';

@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
  ) {}

  async createTempUser() {
    const salt = await bcrypt.genSalt(10);
    const hash = await bcrypt.hash('Password123.', salt);
    return await this.usersService.createUser({
      username: 'leithonenglish',
      email: 'leithonenglish@gmail.com',
      password: hash,
      firstName: 'Leithon',
      lastName: 'English',
    });
  }

  async login(user: any) {
    const { operations, roles, firstName, lastName } = user;
    const payload = { username: user.username, sub: user.userId, operations, roles  };
    return {
      token: this.jwtService.sign(payload),
      operations,
      roles,
      firstName,
      lastName,
    };
  }

  async userExists(username: string): Promise<boolean> {
    const user = await this.usersService.findOneByUsername(username);
    return !!user;
  }

  async getUserByUsername(username: string): Promise<User> {
    return await this.usersService.findOneByUsername(username);
  }

  async validateUser(username: string, password: string): Promise<User> {
    const user = await this.usersService.findOneByUsername(username);
    if (user && !user.locked && user.active) {
      try {
        const matched = await bcrypt.compare(password, user.password);
        if (matched) {
          await this.usersService.updateLastLoggedIn(user.id);
          return user;
        } else {
          const attempts = user.loginAttempts || 0;
          await this.usersService.updateLoginAttemps(user.id, attempts);
        }
      } catch (error) {
        return null;
      }
    }
    return null;
  }
}
